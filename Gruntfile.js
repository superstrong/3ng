module.exports = function (grunt){

    grunt.initConfig({
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['src/BasicViewport.js', 'src/*.module.js','src/*.directive.js','src/*.provider.js'],
                dest: 'build/3ng.js'
            }
        },

        uglify: {
            options: {
                mangle: false
            },
            dist: {
                files: {
                    'build/3ng.min.js': ['build/3ng.js']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('distribute',['concat','uglify']);

}