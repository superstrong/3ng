/**
 * Created by Rockerz on 9/12/14.
 */

var viewport;

angular.module('threeNg').factory('viewport',function (){

    if (!viewport) viewport = new BasicViewport({});
    viewport.renderer.setClearColor(0xCCCCCC);
    return viewport;


});