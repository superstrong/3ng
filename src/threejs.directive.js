/**
 * Created by Rockerz on 9/12/14.
 */
angular.module('threeNg').directive('threeNg',function (viewport){

    var linkFn = function (scope,elm,attrs){

        scope.viewport = viewport
        viewport.setViewportSize(attrs.width,attrs.height)
        elm.append(viewport.domElement);
        viewport.render();

    }

    return {

        link:linkFn


    }

});

