/**
 * Created by Rockerz on 8/12/14.
 */

(function(global){
    'use strict'

    var BasicViewport;

        BasicViewport = function (options){

            var scene,
                camera,
                renderer,
                defaultWidth,
                defaultHeight,
                aspectRatio,
                width,
                height,
                init,
                setViewportSize,
                render,
                add,
                useTrackball,
                trackball
                ;

            defaultWidth = 800;
            defaultHeight = 600;

            init = function init(options){

                width = options.width || defaultWidth;
                height = options.height || defaultHeight;
                aspectRatio = width/height;
                scene = new THREE.Scene();
                camera = new  THREE.PerspectiveCamera( 45, aspectRatio, 0.1, 100000 );
                camera.setLens(35,35)
                camera.zoom = 1;
                camera.position.z = 500
                renderer = new THREE.WebGLRenderer();
                renderer.setSize(width,height);
                useTrackball = options.useTrackball || false;
                if (useTrackball){

                    trackball = new THREE.OrbitControls(camera,renderer.domElement);

                }
                scene.add(camera);

            };

            setViewportSize = function updateViewport (w,h){

                width = w;
                height = h;
                aspectRatio = width/height;
                camera.aspect = aspectRatio;
                renderer.setSize(width,height);


            }

            render = function render (){

                renderer.render(scene,camera);
                window.requestAnimationFrame(render)

            };

            add = function add (geom,warp){

                var obj,

                obj = geom;

                if (warp){

                    obj = new THREE.Object3D();
                    obj.add(geom)
                }

                scene.add(geom);

            }

            this.constructor = init(options);


            return {

                add:add,
                render:render,
                camera:camera,
                scene:scene,
                renderer:renderer,
                domElement:renderer.domElement,
                width:width,
                height:height,
                setViewportSize:setViewportSize

            }

        };

    global.BasicViewport = BasicViewport;

})(this);


;/**
 * Created by Rockerz on 8/12/14.
 */
angular.module('threeNg',[]);;/**
 * Created by Rockerz on 9/12/14.
 */
angular.module('threeNg').directive('threeNg',function (viewport){

    var linkFn = function (scope,elm,attrs){

        scope.viewport = viewport
        viewport.setViewportSize(attrs.width,attrs.height)
        elm.append(viewport.domElement);
        viewport.render();

    }

    return {

        link:linkFn


    }

});

;/**
 * Created by Rockerz on 9/12/14.
 */

var viewport;

angular.module('threeNg').factory('viewport',function (){

    if (!viewport) viewport = new BasicViewport({});
    viewport.renderer.setClearColor(0xCCCCCC);
    return viewport;


});